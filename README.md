# bbmap Singularity container
### Package bbmap Version 39.01
This package includes BBMap, a short read aligner, as well as various other bioinformatic tools. It is written in pure Java, can run on any platform, and has no dependencies other than Java being installed (compiled for Java 6 and higher). All tools are efficient and multithreaded.

Homepage:

https://sourceforge.net/projects/bbmap https://jgi.doe.gov/data-and-tools/software-tools/bbtools/

Package installation using Miniconda3 V4.12.0
All packages are in /opt/miniconda/bin & are in PATH
bbmap Version: 39.01<br>
Singularity container based on the recipe: Singularity.bbmap_v39.01.def

Local build:
```
sudo singularity build bbmap_v39.01.sif Singularity.bbmap_v39.01.def
```

Get image help:
```
singularity run-help bbmap_v39.01.sif
```

Default runscript: bbmap.sh<br>
Usage:
```
./bbmap_v39.01.sif --help
```
or:
```
singularity exec bbmap_v39.01.sif bbmap.sh --help
```

image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>
can be pull (singularity version >=3.3) with:<br>

```
singularity pull bbmap_v39.01.sif oras://registry.forgemia.inra.fr/inter_cati_omics/hackathon_inter_cati_decembre_2022/atelier_1_snakemake_singularity_pangenome/containers/bbmap/bbmap:latest

```

