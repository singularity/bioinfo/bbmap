# distribution based on: debian 10.13 slim
Bootstrap:docker
From:debian:10.13-slim

# container for bbmap v39.01
# Build:
# sudo singularity build bbmap_v39.01.sif Singularity.bbmap_v39.01.def


%environment
export LC_ALL=C
export LC_NUMERIC=en_GB.UTF-8
export PATH="/opt/miniconda/bin:$PATH"

#%labels
#COPYRIGHT INRAe GAFL 2022
#AUTHOR Jacques Lagnel
#VERSION 1.1
#LICENSE MIT
#DATE_MODIF MYDATEMODIF
#bbmap (v39.01)

%help
Container for bbmap version 39.01
This package includes BBMap, a short read aligner, as well as various other bioinformatic tools. It is written in pure Java, can run on any platform, and has no dependencies other than Java being installed (compiled for Java 6 and higher). All tools are efficient and multithreaded.
Homepage:
https://sourceforge.net/projects/bbmap https://jgi.doe.gov/data-and-tools/software-tools/bbtools/

Package installation using Miniconda3 V4.11.0
All packages are in /opt/miniconda/bin & are in PATH
Default runscript: bbmap.sh

Usage:
    ./bbmap_v39.01.sif --help
    or:
    singularity exec bbmap_v39.01.sif bbmap.sh --help

%runscript
    #default runscript: bbmap.sh passing all arguments from cli: $@
    exec /opt/miniconda/bin/bbmap.sh "$@"

%post
    #essential stuff but minimal
    apt-get update
    #for security fixe:
    #apt upgrade -y
    apt-get install -y wget bzip2

    #install conda
    cd /opt

    #miniconda
    # miniconda based on python2
    #wget https://repo.continuum.io/miniconda/Miniconda2-4.7.12-Linux-x86_64.sh -O miniconda.sh
    # miniconda3: based on python3
    wget https://repo.anaconda.com/miniconda/Miniconda3-py39_4.12.0-Linux-x86_64.sh -O miniconda.sh

    #install conda
    bash miniconda.sh -b -p /opt/miniconda
    export PATH="/opt/miniconda/bin:$PATH"
    #add channels
    conda config --add channels defaults
    conda config --add channels bioconda
    conda config --add channels conda-forge

    #install bbmap

    conda install -y -c bioconda bbmap=39.01

    #cleanup
    conda clean -y --all
    rm -f /opt/miniconda.sh
    apt autoremove --purge
    apt clean

%test
    exec /opt/miniconda/bin/bbmap.sh -h
